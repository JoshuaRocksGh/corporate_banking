<?php

use App\Http\Controllers\Authentication\LoginController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\transferController;
use App\Http\Controllers\loansController;
use App\Http\Controllers\accountServiceController;
use App\Http\Controllers\Approvals\ApprovedController;
use App\Http\Controllers\Payments\paymentController;
use App\Http\Controllers\Approvals\PendingController;
use App\Http\Controllers\Approvals\RejectedController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\transfer\addBeneficiaryTransferController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('login');
// });

//route to display the home page of the portal
Route::get('/home',[HomeController::class, 'index'])->name('home');

//route to return the login page of the corporate internet banking platforms
Route::get('/login', [LoginController::class, 'login'])->name('login');

//route to return the retur password page
Route::get('/reset-password', [loginController::class, 'reset_password'])->name('reset-password');

//route to take care of the forget password page
Route::get('/forget-password', [loginController::class, 'forget_password'])->name('forget-password');

// //route to control the transfers screen
// Route::get('/transfer', [transferController::class, 'transfer'])->name('transfer');

//route to control the add beneficiary screen
Route::get('/payment-add-beneficiary',[paymentController::class, 'addBeneficiary'])->name('payment-add-beneficiary');

//route to control the saved beneficiary screen
Route::get('/saved-beneficiary',[paymentController::class, 'savedBeneficiary'])->name('saved-beneficiary');

//route to control the utility payment screen
Route::get('/utility-Payment',[paymentController::class, 'utilityPayment'])->name('utility-Payment');

//route to control the mobile money payment
Route::get('/one-time-payment',[paymentController::class, 'one_time_payment'])->name('one-time-payment');

//route to control the mobile money payment
Route::get('/mobile-money-beneficiary',[paymentController::class, 'mobileMoneyBeneficiary'])->name('mobile-money-beneficiary');

//route to control the loans screen
Route::get('/loan',[loansController::class, 'loan'])->name('loan');

//route to control the loans screen
Route::get('/openAccount',[accountServiceController::class, 'openAccount'])->name('openAccount');

//route to conrol the ownAccountTransfer screen
Route::get('/own-bank-account-transfer',[transferController::class,'own_bank_account_transfer'])->name('own-bank-account-transfer');

//route to conrol the ownAccountTransfer screen
Route::get('/same-bank-account-transfer',[transferController::class,'same_bank_account_transfer'])->name('same-bank-account-transfer');

//route to control the otherlocalAccountTransfer screen
Route::get('/other-local-transfer',[transferController::class,'other_local_transfer'])->name('other-local-transfer');

//route to control the add beneficiary screen
Route::get('/add-beneficiary',[transferController::class,'add_beneficiary'])->name('add-beneficiary');

//route to control the add beneficiary of the own bank screen
Route::get('/add-beneficiary/own-account-beneficiary',[addBeneficiaryTransferController::class,'add_own_account_beneficiary'])->name('own-account-beneficiary');

//route to control the add beneficiary of the same bank screen
Route::get('/add-beneficiary/same-bank-beneficiary',[addBeneficiaryTransferController::class,'add_beneficiary_same_bank'])->name('same-bank-beneficiary');

//route to control the add beneficiary of the other local bank screen
Route::get('/add-beneficiary/other-local-bank-beneficiary',[addBeneficiaryTransferController::class,'add_beneficiary_other_local_bank'])->name('other-local-bank-beneficiary');

//route to control the international beneficiary account screen
Route::get('/add-beneficiary/international-bank-beneficiary',[addBeneficiaryTransferController::class,'add_beneficiary_international_bank'])->name('international-bank-beneficiary');

//route to control the pending screen
Route::get('/pending-approvals',[PendingController::class,'pendingApprovals'])->name('pending-approvals');

//route to control the rejected screen
Route::get('/rejected-approvals',[RejectedController::class,'rejectedApprovals'])->name('rejected-approvals');

//route to control the rejected screen
Route::get('/approved-approvals',[ApprovedController::class,'approvedApprovals'])->name('approved-approvals');

//route to control the reset password screen
Route::get('/reset-password',[ResetPasswordController::class,'reset_password'])->name('reset-password');

//route to control the reset password screen
Route::get('/email-reset',[ResetPasswordController::class,'pass_email'])->name('email-reset');

//route to control the rejected screen
Route::get('/approvals-pending-transfer-details',[PendingController::class,'approvals_pending_transfer_details'])->name('approvals-pending-transfer-details');
//route for the authente users of the web portal
// Auth::routes();



