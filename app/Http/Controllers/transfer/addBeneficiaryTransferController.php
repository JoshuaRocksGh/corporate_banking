<?php

namespace App\Http\Controllers\transfer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class addBeneficiaryTransferController extends Controller
{
    //method to add new beneficiary.
    public function addBeneficiary()
    {
        return view('pages.transfer.add_beneficiary');
    }

    //method to add new beneficiary.
    public function add_own_account_beneficiary()
    {
        return view('pages.transfer.add_own_account_beneficiary');
    }


    //method to return the add beneficiary with the same beneficiary screen
    public function add_beneficiary_same_bank(){
        return view('pages.transfer.same_bank_beneficiary');
    }

    //method to return the add beneficiary with the other local bank screen
    public function add_beneficiary_other_local_bank(){
        return view('pages.transfer.add_other_local_bank_beneficiary');
    }

    //method to return the add beneficiary with interntional bank screen
    public function add_beneficiary_international_bank(){
        return view('pages.transfer.add_international_beneficiary');
    }

}
